import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileWriter;
import java.io.IOException;


class GUI extends JFrame implements ActionListener {

    private TextField textField;
    private TextField textField2;
    private TextArea textArea;
    private JButton writeButton;
    private String fileName;
    private String message;

    public GUI() {
        setTitle("Examen");
        setLayout(new FlowLayout());
        textField = new TextField();
        textField2 = new TextField();
        writeButton = new JButton("Scrie");
        textArea = new TextArea();
        writeButton.addActionListener(this);
        add(textField);
        add(textField2);
        add(writeButton);
        add(textArea);
        setSize(300, 300);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
    }

    public void actionPerformed(ActionEvent ae) {

        fileName = textField.getText().toString();
        message = textField2.getText().toString();
    }


    public static void main(String[] args) throws IOException {
        GUI gui = new GUI();
        FileWriter mywriter = new FileWriter(gui.fileName);
        mywriter.write(gui.message);
        mywriter.close();
    }

}